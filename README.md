# Presentation

This repository is a simple copy of the Oculus Integration SDK version 46. It was imported in Unity 2022 and pushed here. The main advantage of this method is that we can add this repository as submodule in multiple Unity repositories and therefore save space in the repository.

# How to use

Add this repository as a submodule in your Unity's project git folder. The content must be under Assets/Oculus. In otherwords, set the relative path of the submodule to be "Assets/Oculus" when adding the submodule to your project.
